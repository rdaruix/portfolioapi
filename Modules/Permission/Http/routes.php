<?php

Route::group(['middleware' => 'web', 'prefix' => 'permission', 'namespace' => 'Modules\Permission\Http\Controllers'], function()
{
    Route::get('/', 'PermissionController@index');
});

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'v1/role', 'namespace' => 'Modules\Permission\Http\Controllers'], function(){

    Route::get('/getRoles', 'PermissionController@getRoles');
});