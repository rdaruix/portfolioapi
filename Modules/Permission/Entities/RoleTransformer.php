<?php
/**
 * Created by PhpStorm.
 * User: juan
 * Date: 1/10/18
 * Time: 3:31 PM
 */

namespace Modules\Permission\Entities;
use League\Fractal;
use Spatie\Permission\Models\Role;


class RoleTransformer extends Fractal\TransformerAbstract
{
    public function transform(Role $role){
        return [
            'id'           => (int) $role->id,
            'name'         => $role->name
        ];
    }
}