<?php

Route::group(['prefix' => 'v1/user/public', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::get('/', 'UserController@getUser');
    Route::post('/login', 'UserController@doLogin');
});

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'v1/user', 'namespace' => 'Modules\User\Http\Controllers'], function(){

    Route::post('/addUser', 'UserController@addUser');
    Route::get('/removeUser/{id}', 'UserController@removeUser');
    Route::get('/getUserById/{id}', 'UserController@getUserById');
    Route::get('/getUsers', 'UserController@getUsers');
    Route::post('/changePass', 'UserController@changePassword');
});
