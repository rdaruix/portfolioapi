<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\User\Entities\User;
use JWTAuth;
use Modules\User\Entities\UserTransformer;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Support\Facades\URL;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('user::index');
    }

    public function  addUser(Request $request){

        $path = public_path();

        $request['password'] = bcrypt($request['password']);
        $user = $request->all();

        if($request->hasFile('image')){
            $imagem = $request->file('image');
            $num = rand(1111,9999);
            $dir = $path."/img";
            $ex = $imagem->guessClientExtension();
            $nomeImagem = "imagem_".$num.".".$ex;
            $imagem->move($dir,$nomeImagem);
            $user['image'] = URL::to('/public/img')."/".$nomeImagem;
        }

        if(isset($user['id'])){
            $u = User::find($user['id']);
            $u->update($user);
            $u->syncRoles($request['role']);
        }else{
            User::create($user)->assignRole($request['role']);
        }

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }

    public function removeUser($id)
    {
        $user = User::find($id);
        $userRoles = $user->getRoleNames();

        foreach ($userRoles as $role){
            $user->removeRole($role);
        }

        $user->delete();

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }

    public function getUserById($id){
        $user = User::find($id);

        $userFormatted = Fractal::create()
            ->item($user)
            ->transformWith(UserTransformer::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $userFormatted
        ], 200);
    }
    public function getUser(){

	    $user = JWTAuth::parseToken()->authenticate();
    	$account = User::where('email', $user['email'])->first();

        try {

            if(is_null($account)){

                return response()->json([
                    'status' => 400,
                    'slug' => 'account-not-found',
                    'message' => 'Account not found.',
                ], 400);
            }

        } catch (TokenExpiredException $e) {

            return response()->json([
                'status' => 400,
                'slug' => 'token-has-expired',
                'message' => 'Token has expired',
            ], 400);

        } catch (TokenInvalidException $e){
            return response()->json([
                'status' => 400,
                'slug' => 'token-is-invalid',
                'message' => 'Token is invalid',
            ], 400);
        }catch (JWTException $e){
            return response()->json([
                'status' => 400,
                'slug' => 'token-not-exists',
                'message' => 'Token not exists',
            ], 400);
        }

        $userFormatted = Fractal::create()
            ->item($account)
            ->transformWith(UserTransformer::class)
            ->serializeWith(new Serializer())
            ->toArray();
        return response()->json($userFormatted, 200);
    }

    public function doLogin(Request $request){

    	$userLogin = User::where('email', $request['email'])->first();
    	if($userLogin){
			$token = JWTAuth::fromUser($userLogin);

		    User::where('email', $request['email'])->update(['remember_token'=>1]);
		    return response()->json([
			    'status' => 200,
			    'slug' => 'response-ok',
			    'message' => 'Response ok.',
			    'token' => 'Bearer '.$token,
			    'userLogged' => $userLogin['email']
		    ], 200);
	    }else{

		    return response()->json([
			    'status' => 400,
			    'slug' => 'without-user',
			    'message' => 'No user found'
		    ], 400);
	    }

    }

    public function getUsers(){

        $data = User::all();

        $usersFormatted = Fractal::create()
            ->collection($data)
            ->transformWith(\Modules\User\Entities\UserTransformer::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $usersFormatted
        ], 200);
    }

    public function changePassword(Request $request){

        $user = User::find($request['userId']);

        $request['password'] = bcrypt($request['password']);
        $user->password = $request['password'];
        $user->save();

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }
}
