<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16/11/2017
 * Time: 19:47
 */

namespace Modules\User\Entities;
use League\Fractal;
use Modules\User\Entities\User;


class UserTransformer  extends Fractal\TransformerAbstract{

	public function transform(User $user){
		return [
			'id'           => (int) $user->id,
			'name'         => $user->name,
			'email'        => $user->email,
            'image'        => $user->image,
            'password'     => $user->password,
            'roles'        => $user->getRoleNames(),
            'permissions'  => $user->getPermissionsViaRoles()->toArray(),
			'created_at'   => $user->created_at,
			'updated_at'   => $user->updated_at
		];
	}
}