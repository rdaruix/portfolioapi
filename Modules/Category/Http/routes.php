<?php

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'v1/category', 'namespace' => 'Modules\Category\Http\Controllers'], function()
{
	Route::get('/categories', 'CategoryController@show');
});
