<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;

use Modules\Category\Entities\Category as Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return "teste";
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('category::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
    	$category = Category::all();
    	if(is_null($category)){
            return response()->json([
                'status' => 404,
                'slug' => 'categories-not-found',
                'message' => 'Categories not found'
            ], 404);
        }else{
            return response()->json([
                'status' => 200,
                'slug' => 'response-ok',
                'message' => 'Response ok.',
                'data' => $category
            ], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('category::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
