<?php

namespace Modules\Category\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Category\Entities\Category;

class CategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Category::create(['name'=>'Site Institucional']);
        Category::create(['name'=>'Landing Page']);
        Category::create(['name'=>'E-mail Makerting']);
        Category::create(['name'=>'Sistemas']);
        Category::create(['name'=>'Sites em Wordpress']);
    }
}
