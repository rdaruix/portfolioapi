<?php

namespace Modules\Technology\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Technology\Entities\Technology as Technology;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;

class TechnologyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('technology::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('technology::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $technology = $request->all();

        Technology::create($technology);

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
	    $technologies = Technology::all();
	    $technologiesFormatted = Fractal::create()
		    ->collection($technologies)
		    ->transformWith(\Modules\Technology\Entities\TechnologyTransformer::class)
		    ->serializeWith(new Serializer())
		    ->toArray();

	    return response()->json([
		    'status' => 200,
		    'slug' => 'response-ok',
		    'message' => 'Response ok.',
		    'data' => $technologiesFormatted
	    ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('technology::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        Technology::find($id)->delete();

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }
}
