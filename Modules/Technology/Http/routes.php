<?php

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'v1/technology', 'namespace' => 'Modules\Technology\Http\Controllers'], function()
{
    Route::get('/', 'TechnologyController@index');
    Route::get('/technologies', 'TechnologyController@show');
    Route::post('/addTechnology', 'TechnologyController@store');
    Route::get('/deleteTechnology/{id}', 'TechnologyController@destroy');
});
