<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 05/11/2017
 * Time: 17:12
 */

namespace Modules\Technology\Entities;
use Modules\Technology\Entities\Technology as Technology;
use League\Fractal;


class TechnologyTransformer extends Fractal\TransformerAbstract {
	public function transform(Technology $technology){
		return [
			'id'           => (int) $technology->id,
			'name'         => $technology->name
		];
	}
}