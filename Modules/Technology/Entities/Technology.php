<?php

namespace Modules\Technology\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Technology extends Model
{
    protected $fillable = ['name'];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $hidden = [
		'pivot',
	];

	public function getJobs(){
		return $this->belongsToMany('Modules\Job\Entities\Job', 'job_technology', 'technology_id', 'job_id');
	}


}
