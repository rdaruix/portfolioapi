<?php

namespace Modules\Job\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\URL;
use Modules\Technology\Entities\Technology;
use Modules\Technology\Http\Controllers\TechnologyController;
use Nwidart\Modules\Facades\Module;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;
use Modules\Job\Entities\Job as Job;
use Illuminate\Support\Collection;
use Carbon\Carbon;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('job::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('job::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

	    $path = public_path();

	    $job = $request->all();

	    if($request->hasFile('image')){
		    $imagem = $request->file('image');
		    $num = rand(1111,9999);
		    $dir = $path."/img";
		    $ex = $imagem->guessClientExtension();
		    $nomeImagem = "imagem_".$num.".".$ex;
		    $imagem->move($dir,$nomeImagem);
            $job['image'] = URL::to('/public/img')."/".$nomeImagem;
	    }

	    $dt = Carbon::createFromFormat('d-m-Y', $job['date_job']);
        $job['date_job'] = $dt->toDateString();

        if(isset($job['id'])){
            $j = Job::find($job['id']);
            $j->getTechnologies()->sync($request['technologies']);
            $j->update($job);
        }else{
            $jb = Job::create($job);
            $j = Job::find($jb->id);
            $area = json_decode($request['technologies']);
            foreach ($area as $technology){
                $j->getTechnologies()->attach($technology);
            }
        }

	    return response()->json([
		    'status' => 200,
		    'slug' => 'response-ok',
		    'message' => 'Response ok.'
	    ], 200);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
	    $jobs = Job::all();
	    $jobsFormatted = Fractal::create()
		    ->collection($jobs)
		    ->transformWith(\Modules\Job\Entities\JobTransformer::class)
		    ->serializeWith(new Serializer())
		    ->toArray();

	    return response()->json([
		    'status' => 200,
		    'slug' => 'response-ok',
		    'message' => 'Response ok.',
		    'data' => $jobsFormatted
	    ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $job = Job::find($id);


        $dt = Carbon::createFromFormat('Y-m-d', $job['date_job']);

        $job['date_job'] = $dt->format('d-m-Y');

        $jobFormatted = Fractal::create()
            ->item($job)
            ->transformWith(\Modules\Job\Entities\JobTransformer::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $jobFormatted
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
    	Job::find($id)->delete();

	    return response()->json([
		    'status' => 200,
		    'slug' => 'response-ok',
		    'message' => 'Response ok.'
	    ], 200);
    }

    public function getLastJob(){
        $data = Job::all();
        $lastJob =  collect($data)->last();

        $jobFormatted = Fractal::create()
            ->item($lastJob)
            ->transformWith(\Modules\Job\Entities\JobTransformer::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $jobFormatted
        ], 200);
    }
}
