<?php

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'v1/job', 'namespace' => 'Modules\Job\Http\Controllers'], function()
{
    Route::get('/', 'JobController@index');
    Route::get('/jobs', 'JobController@show');
    Route::post('/addJob', 'JobController@store');
    Route::get('/deleteJob/{id}', 'JobController@destroy');
    Route::get('/findJob/{id}', 'JobController@edit');
    Route::get('/lastJob', 'JobController@getLastJob');
});
Route::group(['prefix' => 'v1/job/public', 'namespace' => 'Modules\Job\Http\Controllers'], function()
{
    Route::get('/jobs', 'JobController@show');
    Route::get('/findJob/{id}', 'JobController@edit');
});