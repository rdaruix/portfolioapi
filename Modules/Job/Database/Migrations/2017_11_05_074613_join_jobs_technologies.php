<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JoinJobsTechnologies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('job_technology', function (Blueprint $table) {
		    $table->integer('job_id')->unsigned()->default(1);
		    $table->foreign('job_id')->references('id')->on('jobs');
		    $table->integer('technology_id')->unsigned()->default(1);
		    $table->foreign('technology_id')->references('id')->on('technologies');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
