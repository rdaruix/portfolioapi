<?php

namespace Modules\Job\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    protected $fillable = ['name','description', 'date_job', 'image', 'link'];
	use SoftDeletes;
	protected $dates = ['deleted_at'];



    public function getCategory(){
    	return $this->belongsTo('Modules\Category\Entities\Category', 'category_id');
    }

    public function getTechnologies(){
    	return $this->belongsToMany('Modules\Technology\Entities\Technology', 'job_technology', 'job_id', 'technology_id');
    }
}
