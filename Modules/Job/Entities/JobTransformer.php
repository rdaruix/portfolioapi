<?php
namespace Modules\Job\Entities;
use Modules\Job\Entities\Job;
use League\Fractal;

class JobTransformer extends Fractal\TransformerAbstract{
	public function transform(Job $job){
		return [
			'id'           => (int) $job->id,
			'name'         => $job->name,
			'description'  => $job->description,
			'date_job'     => date($job->date_job),
            'image'        => $job->image,
            'link'         => $job->link,
			'category'     => $job->getCategory()->getEager()->first(),
			'technologies' => $job->getTechnologies()->getEager()->all(),
			'created_at'   => $job->created_at,
			'updated_at'   => $job->updated_at
		];
	}

	protected $availableIncludes = [
		'category', 'technologies'
	];
}
