<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'create']);
        Permission::create(['name' => 'edit']);
        Permission::create(['name' => 'delete']);
        Permission::create(['name' => 'view']);

        // create roles and assign existing permissions
        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('create');
        $role->givePermissionTo('edit');
        $role->givePermissionTo('delete');
        $role->givePermissionTo('view');

        $role = Role::create(['name' => 'client']);
        $role->givePermissionTo('edit');
        $role->givePermissionTo('view');

        $role = Role::create(['name' => 'prospector']);
        $role->givePermissionTo('create');
        $role->givePermissionTo('edit');
        $role->givePermissionTo('view');
    }
}
